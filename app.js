
var express = require('express');

var app = express();

// view engine setup
app.set('view engine', 'ejs');

app.get('/', function(req, res) {
    res.render('/views/index');
});

app.listen(8080);