# Javascipt
This is my project web practice Javascript, HTML, CSS and Bootstrap.

## Clone repository from gitlab to computer 
step 1: cd to the folder you need to use
```
ex: you use folder name 'project' in folder D

cd D:\project
```

step 2: clone repository we use the command:
```
git clone https://gitlab.com/nguyendangkhoa150600/javascript-training
```

## Checkout to master branch
```
git checkout develop
```

# How to run my project
First: install parcel
```
# Using npm run: 
npm install --save-dev parcel

# Using Yarn:
yarn add --dev parcel
```

Second: run project
```
# Using npm run: 
npx parcel index.html

# Using Yarn:
yarn parcel index.html
```

Finally: 
Now open http://localhost:1234/ in your browser to see the HTML file you created above

