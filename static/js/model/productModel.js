(function(window) {
    'use strict';

    /**
     * Creates a new Model instance and hooks up the storage.
     * @param {object} storage A reference to the client side storage class
     */
    function ProductModel(storage) {
        this.storage = storage;
    }


    /**
     * Finds and returns a model in storage
     */
    ProductModel.prototype.read = function(query, callback) {
        var queryType = typeof query;
        callback = callback || function() {};
        if (queryType === 'function') {
            callback = query;
            return this.storage.findAll(callback);
        } else if (queryType === 'string' || queryType === 'number') {
            query = parseInt(query, 10);
            this.storage.find({ id: query }, callback);
        } else {
            this.storage.find(query, callback);
        }
    }

    /**
     * Creates a new product model
     * @param {object} [addNewProduct] The title of the task
     * @param {function} [callback] The callback to fire after the model is created
     */
    ProductModel.prototype.create = function(addNewProduct, callback) {
        var newProduct = {
            image: addNewProduct.image || '',
            name: addNewProduct.name || '',
            price: addNewProduct.price || '',
            type: addNewProduct.type || ''
        };

        this.storage.save(newProduct, callback);
    };

    /**
     * Removes a model from storage
     * @param {number} id The ID of the model to remove
     * @param {function} callback The callback to fire when the removal is complete.
     */
    ProductModel.prototype.remove = function(id, callback) {
        this.storage.remove(id, callback);
    };

    /**
     * Updates a model by giving it an ID, data to update, and a callback to fire when
     * the update is complete.
     * @param {number} id The id of the model to update
     * @param {object} data The properties to update and their new value
     * @param {function} callback The callback to fire when the update is complete.
     */
    ProductModel.prototype.update = function(id, data, callback) {
        this.storage.save(data, callback, id);
    };

    /**
     * @param {object} data The properties to get input value
     * @param {function} callback The callback to fire when the update is complete.
     */
    ProductModel.prototype.search = function(data, callback) {
        this.storage.findProduct(data, callback);
    };

    // Export to window
    window.app = window.app || {};
    window.app.ProductModel = ProductModel;
})(window);