(function(window) {
    'use strict';

    /**
     * Creates a new Model instance and hooks up the storage.
     * @param {object} storage A reference to the client side storage class
     */
    function AccountModel(storage) {
        this.storage = storage;
    }

    /**
     * Creates a new account model
     * @param {object} [account] 
     * @param {function} [callback] The callback to fire after the model is created
     */
    AccountModel.prototype.createAccount = function(account, callback) {
        var newAccount = {
            userName: account.userName || '',
            password: account.password || ''
        };
        this.storage.saveAccount(newAccount, callback);
    };

    AccountModel.prototype.findAccount = function(data, callback) {
        this.storage.findAccount(data, callback);
    };

    // Export to window
    window.app = window.app || {};
    window.app.AccountModel = AccountModel;
})(window);