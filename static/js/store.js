(function(window) {
    'use strict';

    function Store(dbProduct, dbAccount, callback) {
        callback = callback || function() {};

        this._dbName = dbProduct;
        this._dbdbAccount = dbAccount;

        if (!localStorage.getItem(dbProduct)) {
            var products = [];

            localStorage.setItem(dbProduct, JSON.stringify(products));
            callback.call(this, JSON.parse(localStorage.getItem(dbProduct)));

        } else if (!localStorage.getItem(dbAccount)) {
            var accounts = [];

            localStorage.setItem(dbAccount, JSON.stringify(accounts));
            callback.call(this, JSON.parse(localStorage.getItem(dbAccount)));
        }

    }

    //Save Product
    Store.prototype.save = function(updateData, callback, id) {
        var products = JSON.parse(localStorage.getItem(this._dbName));
        callback = callback || function() {};

        if (id) {
            for (var i = 0; i < products.length; i++) {
                if (products[i].id == id) {
                    for (var key in updateData) {
                        products[i][key] = updateData[key];
                    }
                    break;
                }
            }
            localStorage.setItem(this._dbName, JSON.stringify(products));
            callback.call(this, products);
        } else {
            updateData.id = new Date().getTime();

            products.push(updateData);
            localStorage.setItem(this._dbName, JSON.stringify(products));
            callback(products);
        }
    };

    //Find Product
    Store.prototype.find = function(query, callback) {
        if (!callback) {
            return;
        }

        var products = JSON.parse(localStorage.getItem(this._dbName));

        callback.call(this, products.filter(function(product) {
            for (var q in query) {
                if (query[q] !== product[q]) {
                    return false;
                }
            }
            return true;
        }));
    };

    //Find all Product
    Store.prototype.findAll = function(callback) {
        callback = callback || function() {};
        callback.call(this, JSON.parse(localStorage.getItem(this._dbName)));
    };

    //Remove product
    Store.prototype.remove = function(id, callback) {
        var products = JSON.parse(localStorage.getItem(this._dbName));

        for (var i = 0; i < products.length; i++) {
            if (products[i].id == id) {
                products.splice(i, 1);
                break;
            }
        }

        localStorage.setItem(this._dbName, JSON.stringify(products));
        callback.call(this, products);
    };

    //Search Product
    Store.prototype.findProduct = function(search, callback) {
        var products = JSON.parse(localStorage.getItem(this._dbName));

        callback.call(this, products.filter(function(product) {
            for (var i = 0; i < products.length; i++) {
                dbProduct = product.name.toLowerCase()
                if (search == '') {
                    return products;

                } else if (dbProduct.includes(search.toLowerCase())) {
                    return product.name;
                }
            }

        }));

    };

    //save new account
    Store.prototype.saveAccount = function(data, callback) {
        var accounts = JSON.parse(localStorage.getItem(this._dbdbAccount));

        callback = callback || function() {};

        accounts.push(data);
        localStorage.setItem(this._dbdbAccount, JSON.stringify(accounts));
    };

    //dbAccount with account
    Store.prototype.findAccount = function(data, callback) {
        var accounts = JSON.parse(localStorage.getItem(this._dbdbAccount));

        for (var i = 0; i < accounts.length; i++) {
            if (accounts[i].userName == data.userName && accounts[i].password == data.password) {
                alert('Login success !!!')
                callback.call(this, accounts);

            } else if (accounts[i].userName == data.userName && accounts[i].password != data.password) {
                alert('Password fail !!!');

            } else {
                alert('Login fail !!!');
            }
        }
    };

    // Export to window
    window.app = window.app || {};
    window.app.Store = Store;
})(window);