(function(window) {
    'use strict';

    function View(template) {
        this.template = template;

        //Event
        this.$showSignInBtn = qs('.show-sign-in-btn');
        this.$showSignUpBtn = qs('.show-sign-up-btn');
        this.$signInBtn = qs('.btn-sign-in');
        this.$addProductBtn = qs('.add-product-btn')
        this.$deleteProductBtn = qs('.btn-danger')
        this.$modalAddBtn = qs('.btn-modal-add')
        this.$searchProductBtn = qs('.btn-search-product')

        //Form
        this.$signInForm = qs('.sign-in-form');
        this.$signUpForm = qs('.sign-up-form');
        this.$productForm = qs('.product-form');
        this.$listProducts = qs('.list-products');
        this.$addProduct = qs('.add-product');
        this.$modalAdd = qs('.modal-add-product');
        this.$modalEdit = qs('.modal-edit-product')
        this.$loginForm = qs('.login-form')

        this.$indicator = qs('.indicator');
    }


    View.prototype.render = function(viewCmd, parameter) {
        var self = this;

        var viewCommands = {
            showSignInPage: function() {
                self.$signInForm.innerHTML = self.template.showSignIn();
                self.$signUpForm.innerHTML = '';
                self.$indicator.style.transform = 'translateX(0px)';
            },

            showSignUpPage: function() {
                self.$signUpForm.innerHTML = self.template.showSignUp();
                self.$signInForm.innerHTML = '';
                self.$indicator.style.transform = 'translateX(115px)';
            },

            closeModal: function() {
                self.$modalAdd.style.display = 'none';
                self.$modalEdit.style.display = 'none';
            },

            showProductForm: function() {
                self.$productForm.style.display = 'block';
                self.$loginForm.style.display = 'none';
            },

            showAddForm: function() {
                self.$modalAdd.style.display = 'block';
                self.$modalAdd.innerHTML = self.template.showAddForm();
            },

            showListProduct: function() {
                self.$listProducts.innerHTML = self.template.show(parameter);
            },

            removeProduct: function() {
                self._removeProduct(parameter);
            },

            showEditForm: function() {
                self.$modalEdit.style.display = 'block';
                self.$modalEdit.innerHTML = self.template.showEditForm();
            },

            editProduct: function() {
                var editProduct = {
                    id: parameter.id,
                    image: parameter.image,
                    name: parameter.name,
                    price: parameter.price,
                    type: parameter.type
                }
                self._editProduct(editProduct);
            },

            editProductSubmit: function() {}
        };

        viewCommands[viewCmd]();
    };

    View.prototype.bind = function(event, handler) {
        var self = this;
        var callback = function(e) {
            e.stopPropagation();
            e.preventDefault();
            handler(e);
        };

        if (event === 'showSignInPage') {
            $on(self.$showSignInBtn, 'click', callback);

        } else if (event === 'signIn') {
            $delegate(self.$signInForm, '.btn-sign-in', 'click', function() {
                handler({
                    userName: document.querySelector(".user-name-sign-in").value,
                    password: document.querySelector(".password-sign-in").value
                });
            });

        } else if (event === 'showSignUpPage') {
            $on(self.$showSignUpBtn, 'click', callback);

        } else if (event === 'newAccount') {
            $delegate(self.$signUpForm, '.btn-sign-up', 'click', function(e) {

                var newAccount = {
                    userName: document.querySelector('.user-name-sign-up').value,
                    password: document.querySelector('.password-sign-up').value,
                    rePassword: document.querySelector('.re-password').value,
                }

                e.stopPropagation();
                e.preventDefault();
                handler(newAccount)
            });

        } else if (event === 'closeAddModal') {
            $delegate(self.$modalAdd, '.btn-close-modal', 'click', callback);
            $delegate(self.$modalAdd, '.close-button', 'click', callback);

        } else if (event === 'closeEditModal') {
            $delegate(self.$modalEdit, '.btn-close-modal', 'click', callback);
            $delegate(self.$modalEdit, '.close-button', 'click', callback);

        } else if (event === 'showAddForm') {
            $on(self.$modalAddBtn, 'click', callback);

        } else if (event === 'showEditForm') {
            $delegate(self.$listProducts, '.btn-edit-product', 'click', function() {
                handler({ id: self._productId(this) });
            });

        } else if (event === 'newProduct') {
            $delegate(self.$modalAdd, '.add-product-btn', 'click', function() {

                var newProduct = {
                    image: document.querySelector('.image-product').value,
                    name: document.querySelector('.name-product').value,
                    price: document.querySelector('.price-product').value,
                    type: document.querySelector('.type-product').value,
                }

                handler(newProduct);
            });

        } else if (event === 'productRemove') {
            $delegate(self.$listProducts, '.btn-danger', 'click', function() {
                handler({ id: self._productId(this) });
            });

        } else if (event === 'editProductSubmit') {
            $delegate(self.$listProducts, '.btn-edit-product', 'click', function(event) {
                var idProduct = event.target.dataset.id

                $delegate(self.$modalEdit, '.btn-update-product', 'click', function() {
                    var obj = {
                        id: idProduct,
                        image: document.querySelector(".modal-image-edit").value,
                        name: document.querySelector(".modal-name-edit").value,
                        price: document.querySelector(".modal-price-edit").value,
                        type: document.querySelector(".modal-type-edit").value
                    }
                    handler(obj);
                });
            });

        } else if (event === 'searchProduct') {
            $on(self.$searchProductBtn, 'click', function() {
                handler({
                    search: document.querySelector(".datatable-search-input").value
                });
            });
        }
    };

    View.prototype._productId = function(element) {
        var section = $parent(element, 'section');
        return parseInt(section.dataset.id, 10);
    };

    View.prototype._removeProduct = function(id) {
        var elem = qs('[data-id="' + id + '"]');

        if (elem) {
            this.$listProducts.removeChild(elem);
        }
        alert("Delete Product success");
    };

    View.prototype._editProduct = function(editProduct) {
        document.querySelector(".modal-image-edit").value = editProduct.image;
        document.querySelector(".modal-name-edit").value = editProduct.name;
        document.querySelector(".modal-price-edit").value = editProduct.price;
        document.querySelector(".modal-type-edit").value = editProduct.type;
    };

    // Export to window
    window.app = window.app || {};
    window.app.View = View;
})(window);