(function(window) {
    'use strict';

    /**
     * Takes a productModel, accountModel and view and acts as the controller between them
     *
     * @constructor
     * @param {object} accountModelThe model instance
     * @param {object} productModel The model instance
     * @param {object} view The view instance
     */

    function Controller(productModel, accountModel, view) {
        var self = this;
        this.productModel = productModel;
        this.accountModel = accountModel;
        this.view = view;

        this.view.bind('showSignInPage', function() {
            self.handleShowSignInPage();
        });

        this.view.bind('signIn', function(account) {
            self.handleSignInPage(account.userName, account.password);
        });

        this.view.bind('showSignUpPage', function() {
            self.handleShowSignUpPage();
        });

        this.view.bind('newAccount', function(newAccount) {
            var account = {
                userName: newAccount.userName,
                password: newAccount.password,
                rePassword: newAccount.rePassword
            }
            self.handleSignUpPage(account);
        });

        this.view.bind('showAddForm', function() {
            self.handleShowAddForm();
        });

        this.view.bind('closeAddModal', function() {
            self.handleCloseModalForm();
        });

        this.view.bind('closeEditModal', function() {
            self.handleCloseModalEditForm();
        });

        this.view.bind('showEditForm', function(event) {
            self.handleShowEditForm(event);
            self.handleEditProduct(event.id);
        });

        self.view.bind('newProduct', function(newProduct) {
            var addNewProduct = {
                image: newProduct.image,
                name: newProduct.name,
                price: newProduct.price,
                type: newProduct.type
            }
            self.addProduct(addNewProduct);
        });

        self.view.bind('productRemove', function(product) {
            self.removeProduct(product.id);
        });

        this.view.bind('editProductSubmit', function(product) {
            var edit = {
                id: product.id,
                image: product.image,
                name: product.name,
                price: product.price,
                type: product.type
            }
            self.handleEditProductSubmit(edit)
        });

        this.view.bind('searchProduct', function(product) {
            self.handleSearchProduct(product.search)
        });
    }

    /**
     * Loads and initialises the view
     */
    Controller.prototype.setViewPage = function(locationHash) {
        var self = this;
        self.view.render('showSignInPage');
    };

    /**
     * event to show Sign_in page
     */
    Controller.prototype.handleShowSignInPage = function() {
        var self = this;
        self.view.render('showSignInPage');
    };

    /**
     * event to check the value entered in Sign-in page and check it with data.
     * And show List products in main page.
     */
    Controller.prototype.handleSignInPage = function(userName, password) {
        var self = this;
        userName = userName.trim();
        password = password.trim();

        self.accountModel.findAccount({ userName: userName, password: password }, function(data) {
            self.view.render('showProductForm', data);
        });

        self.productModel.read(function(data) {
            self.view.render('showListProduct', data);
        });
    };

    /**
     * event to show Sign-up page
     */
    Controller.prototype.handleShowSignUpPage = function() {
        var self = this;
        self.view.render('showSignUpPage');
    };

    /**
     * An event to add new Account in data.
     * when add new Account, Sign-in Page will be displayed
     */
    Controller.prototype.handleSignUpPage = function(account) {
        var self = this;
        if (account.userName == '' || account.password == '') {
            return alert('Please enter username or password!!!');
        } else if (account.password != account.rePassword) {
            return alert('Re Passwords are not the same !!!');
        } else {
            alert('Sign up success !!!')
        }

        self.accountModel.createAccount(account, function() {});
        self.view.render('showSignInPage');
    };

    /**
     * event to show modal add new product.
     */
    Controller.prototype.handleShowAddForm = function() {
        var self = this;
        self.view.render('showAddForm');
    };


    /**
     * event to show modal edit product.
     */
    Controller.prototype.handleShowEditForm = function() {
        var self = this;
        self.view.render('showEditForm');
    };

    /**
     * event to edit product.
     */
    Controller.prototype.handleEditProduct = function(id) {
        var self = this;
        self.productModel.read(id, function(data) {
            var edit = {
                id: id,
                image: data[0].image,
                name: data[0].name,
                price: data[0].price,
                type: data[0].type
            }

            self.view.render('editProduct', edit);
        });
    };


    /**
     * event to close modal form.
     */
    Controller.prototype.handleCloseModalForm = function() {
        var self = this;
        self.view.render('closeModal');
    };

    Controller.prototype.handleCloseModalEditForm = function() {
        var self = this;
        self.view.render('closeModal');
    };

    /**
     * An event to add an product. 
     * Simply pass in the event object and it'll handle the DOM insertion and saving of the new product.
     */
    Controller.prototype.addProduct = function(addNewProduct) {
        var self = this;

        if (addNewProduct.name === '') {
            return;
        }

        self.productModel.create(addNewProduct, function() {
            self.view.render('closeModal');
        });

        self.productModel.read(function(data) {
            self.view.render('showListProduct', data);
        });
    };


    /**
     * By giving it an ID it'll find the DOM element matching that ID,
     * remove it from the DOM and also remove it from storage.
     *
     * @param {number} id The ID of the item to remove from the DOM and
     * storage
     */
    Controller.prototype.removeProduct = function(id) {
        var self = this;
        self.productModel.remove(id, function() {
            self.view.render('removeProduct', id);
        });
    };

    /**
     * event to edit product success.
     */
    Controller.prototype.handleEditProductSubmit = function(edit) {
        var self = this;

        var updateProduct = {
            image: edit.image,
            name: edit.name,
            price: edit.price,
            type: edit.type
        }

        var editProduct = {
            id: edit.id,
            image: edit.image,
            name: edit.name,
            price: edit.price,
            type: edit.type
        }

        self.productModel.update(edit.id, updateProduct, function() {
            self.view.render('editProductSubmit', editProduct);
        });

        self.productModel.read(function(data) {
            self.view.render('showListProduct', data);
            self.view.render('closeModal');
        });
    };

    /**
     * event to search product in data by value input.
     */
    Controller.prototype.handleSearchProduct = function(search) {
        var self = this;
        search = search.trim();

        self.productModel.search(search, function(data) {
            self.view.render('showListProduct', data);
        });
    };

    // Export to window
    window.app = window.app || {};
    window.app.Controller = Controller;
})(window);