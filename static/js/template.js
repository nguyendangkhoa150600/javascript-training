(function(window) {
    'use strict';

    function Template() {
        this.defaultTemplate =
            '<section data-id="{{id}}">' +
            '<div class="form-view-product">' +
            '<div class="row">' +
            '<div class="col-3">' +
            '<img src="{{url-img}}" class="view-product img-product" alt="">' +
            '</div>' +
            '<div class="col-6">' +
            '<p class="product-name"><b>Name:</b> {{name}}</p>' +
            '<p class="product-price"><b>Price:</b> {{price}} VND</p>' +
            '<p class="product-type"><b>Type:</b> {{type}}</p>' +
            '</div>' +
            '<div class="col-3">' +
            '<div class="row">' +
            '<button data-id="{{id}}" class="btn btn-success view-product btn-edit-product">Edit</button>' +

            '</div>' +
            '<div class="row">' +
            '<button class="btn btn-danger view-product">Delete</button>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</section>';
    }

    /**
     * returns Sign-In form for placement in page
     */
    Template.prototype.showSignIn = function() {
        return (
            '<h4 class="text-left">Username</h4>' +
            '<input class="text-box user-name-sign-in" placeholder="Enter Username"/>' +
            '<h4 class="text-left">Password</h4>' +
            '<input class="text-box password-sign-in" type="password" placeholder="Enter Password" name="password" />' +
            '<button id="btn-sign-in" class="btn-sign-in btn-submit mt-4" type="button">Sign in</button>'
        );
    };

    /**
     * returns Sign-up form for placement in page
     */
    Template.prototype.showSignUp = function() {
        return (
            '<h4 class="text-left">Username</h4>' +
            '<input id="user-name-sign-up" class="text-box user-name-sign-up" placeholder="Username" />' +
            '<h4 class="text-left">Password</h4>' +
            '<input id="password-sign-up" class="text-box password-sign-up" type="password" placeholder="Password" />' +
            '<h4 class="text-left">Repeat Password</h4>' +
            '<input id="re-password" class="text-box re-password" type="password" placeholder="Re-Password" />' +
            '<span id="message_check" class="message_check"></span>' +
            '<button id="btn-sign-up" type="submit" class="btn-sign-up btn-submit" type="button">Sign up</button>'
        );
    };

    /**
     * returns Model Add form for placement in page
     */
    Template.prototype.showAddForm = function() {
        return (
            '<form class="modal-content animate" method="post">' +
            '<div class="close-container">' +
            '<span class="close close-button" title="Close Modal">&times;</span>' +
            '</div>' +
            '<h2 class="title-content">Add Product Form</h2>' +
            '<div class="container container-modal">' +

            '<label><b>Link image</b></label>' +
            '<input class="modal-input image-product" type="text" placeholder="Enter the link image Product" name="image" />' +

            '<label><b>Name Product</b></label>' +
            '<input class="modal-input name-product" type="text" placeholder="Enter Name Product" name="name" />' +

            '<label><b>Price</b></label>' +
            '<input class="modal-input price-product" type="number" placeholder="Enter Price" name="price" min="0" />' +

            '<label><b>Type</b></label>' +
            '<br>' +
            '<select class="types-product type-product" name="types" id="types">' +
            '<option value="apple">Apple</option>' +
            '<option value="samsung">SamSung</option>' +
            '<option value="xiaomi">XiaoMi</option>' +
            '</select>' +
            '</div>' +

            '<div class="container container-modal" style="background-color:#f1f1f1">' +
            '<div class="row">' +
            '<div class="col">' +
            '<button type="button" class="btn btn-secondary btn-close-modal btn-modal">Cancel</button>' +
            '</div>' +
            '<div class="col">' +
            '<button id="add-product-btn" type="button" class="btn btn-success add-product-btn btn-modal">Add</button>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</form>'
        );
    };

    /**
     * returns Edit form for placement in page
     */
    Template.prototype.showEditForm = function() {
        return (
            '<form class="modal-content animate"  method="post">' +
            '<div class="close-container">' +
            '<span class="close close-button" title="Close Modal">&times;</span>' +
            '</div>' +
            '<h2 class="title-content">Edit Product Form</h2>' +
            '<div class="container container-modal">' +

            '<label><b>Link image</b></label>' +
            '<input class="modal-input modal-image-edit" type="text" placeholder="Enter the link image Product" name="image" />' +

            '<label><b>Name Product</b></label>' +
            '<input id="modal-name-edit" class="modal-input modal-name-edit" type="text" placeholder="Enter Name Product" name="name-edit"/>' +

            '<label><b>Price</b></label>' +
            '<input class="modal-input modal-price-edit" type="number" placeholder="Enter Price" name="price-edit" min="0"/>' +

            '<label><b>Type</b></label>' +
            '<br>' +
            '<select class="types-product modal-type-edit" name="types-edit" id="types">' +
            '<option value="apple">Apple</option>' +
            '<option value="samsung">SamSung</option>' +
            '<option value="xiaomi">XiaoMi</option>' +
            '</select>' +
            '</div>' +

            '<div class="container container-modal" style="background-color:#f1f1f1">' +
            '<div class="row">' +
            '<div class="col">' +
            '<button type="button" class="btn btn-secondary btn-close-modal btn-modal" >Cancel</button>' +
            '</div>' +
            '<div class="col">' +
            '<button id="product_edit" type="button" class="btn btn-success btn-update-product btn-modal">Edit</button>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</form>'
        );
    };

    /**
     * Creates an <section> HTML string and returns it for placement in page.
     * @param {object} data The object containing keys  to find in the template to replace.
     * @returns {string} HTML String of an <section> element
     */
    Template.prototype.show = function(data) {
        var i, l;
        var view = '';

        for (i = 0, l = data.length; i < l; i++) {
            var template = this.defaultTemplate;
            template = template.replace(/{{id}}/g, data[i].id);
            template = template.replace('{{url-img}}', data[i].image);
            template = template.replace('{{name}}', data[i].name);
            template = template.replace('{{price}}', data[i].price);
            template = template.replace('{{type}}', data[i].type);

            view = view + template;
        }

        return view;
    };

    // Export to window
    window.app = window.app || {};
    window.app.Template = Template;
})(window);