(function() {
    'use strict';

    /**
     * Sets up a brand new Product list and Account list.
     *
     * @param {string} dbProduct The name of new Product list.
     * @param {string} dbAccount The name of new Account list
     */
    function Database(dbProduct, dbAccount) {
        this.storage = new app.Store(dbProduct, dbAccount);
        this.productModel = new app.ProductModel(this.storage);
        this.accountModel = new app.AccountModel(this.storage);
        this.template = new app.Template();
        this.view = new app.View(this.template);
        this.controller = new app.Controller(this.productModel, this.accountModel, this.view);
    }

    var db = new Database('dbProduct', 'dbAccounts');

    function setViewPage() {
        db.controller.setViewPage(document.location.hash);
    };

    $on(window, 'load', setViewPage);
    $on(window, 'hashchange', setViewPage);
})();